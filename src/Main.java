import javax.swing.*;

public class Main {
    public static void main(String[] args) {

        JFrame frame = new JFrame();
        frame.setDefaultCloseOperation(frame.DISPOSE_ON_CLOSE);
        Graficar graficar = new Graficar();
        frame.setContentPane(graficar.getPanel1());
        frame.setTitle("Grafica");
        frame.setSize(600, 600);
        frame.setVisible(true);


    }
}