import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.category.DefaultCategoryDataset;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;


public class Graficar {
    private JPanel panel1;
    private JPanel panelGrafica;
    private JButton button1;

    public Graficar() {
    button1.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {

            double valor1 = 30.23;
            double valor2 = 80.45;
            double valor3 = 60.9;

            DefaultCategoryDataset dataset = new DefaultCategoryDataset();
            dataset.addValue(valor1, "Colombia", "Pais");
            dataset.addValue(valor2, "Argentica", "Pais");
            dataset.addValue(valor3, "Peru", "Pais");

            JFreeChart chart = ChartFactory.createBarChart3D(
                    "Gráfico de Barras GINI",
                    "Paises",
                    "Indice de desigualdad",
                    dataset
            );

            ChartPanel chartPanel = new ChartPanel(chart);
            panelGrafica.removeAll();
            panelGrafica.setLayout(new BorderLayout());
            panelGrafica.add(chartPanel, BorderLayout.CENTER);
            panelGrafica.revalidate();
            panelGrafica.repaint();

        }
    });
}

public JPanel getPanel1(){
    return this.panel1;
}
}
